$(function() {
	var startX = 0, startY = 0,
		 currentX = 0, currentY = 0,
		 zoomLevel = 100,
		 width, ratio, centerX, centerY, transforming = false;

	var $img = $('.img-container img'),
		 $container = $('.img-container');

	var onload = function() {
		  width = $img.width(),
			ratio = $img.height() / width,
			centerX = Math.floor($container.width() / 2),
			centerY = Math.floor($container.height() / 2);
	};

	$img.on('load', onload);
	onload();

	var options = {
		prevent_default: true,
		transform_always_block: true,
		drag_block_horizontal: true,
    drag_block_vertical: true,
    drag_max_touches: 1
	}

	function zoom(delta) {
		zoomLevel += delta;

		var currentWidth = $img.width(),
				currentHeight = currentWidth * ratio,
				offsetX = $img[0].offsetLeft,
				offsetY = $img[0].offsetTop,
				newWidth = Math.floor(width * (zoomLevel / 100));

		$img.width(newWidth);

		var deltaWidth = newWidth - currentWidth,
				deltaHeight = deltaWidth * ratio,
				deltaX = centerX - offsetX,
				deltaY = centerY - offsetY,
				ratioX = deltaX / currentWidth,
				ratioY = deltaY / currentHeight,
				deltaOffsetX = deltaWidth * ratioX,
				deltaOffsetY = deltaHeight * ratioY;

		$img.css({
			top: offsetY - deltaOffsetY,
			left: offsetX - deltaOffsetX
		});
	};

	$img.hammer(options).on('dragstart', function(evt) {evt.preventDefault();});
	$img.hammer(options).on('drag', function(evt) {evt.preventDefault();});

	$container.hammer(options).on('dragstart', function(evt) {
		evt.preventDefault();
		evt.stopPropagation();
		evt.gesture.preventDefault();
		var offset = $img.offset();
		startX = offset.left;
		startY = offset.top;
	});

	$container.hammer(options).on('drag', function(evt) {
		evt.preventDefault();
		evt.stopPropagation();
		evt.gesture.preventDefault();
    if (!transforming) {
  		$img.offset({
  			top: startY + evt.gesture.deltaY,
  			left: startX + evt.gesture.deltaX
  		});
    } else {
      $img.offset({
        top: startY,
        left: startX
      });
    }
	});

	$container.hammer(options).on('transform', function(evt) {
    evt.gesture.preventDefault();
    evt.gesture.stopPropagation();
    evt.preventDefault();
    evt.stopPropagation();
    trforming = true;
    zoom((evt.gesture.scale - 1) * 2);
	});

  $container.hammer(options).on('transformend', function(evt) {
    transforming = false;
    evt.gesture.stopDetect();
  });

	$img.mousewheel(function(event) {
		event.preventDefault();
	});

	$container.mousewheel(function(event, delta) {
		event.preventDefault();
		event.stopPropagation();
		zoom(delta * 10);
	});
});
