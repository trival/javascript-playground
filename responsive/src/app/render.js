responsive.render.randText = function(length) {
  var chars = "qwer tzuiop asdf ghjkl yxcvbnm ,.;-,.?! 23456 17890";
  chars = chars.split("");
  var str = "";
  for (var i = 0; i < length; i++) {
    var index = responsive.math.randInt(chars.length);
    str = str + chars[index];
  };
  return str;
}

responsive.render.colorDiv = function(textLength) {
  var div = document.createElement("div");
  var color = responsive.math.randColor();
  div.textContent = responsive.render.randText(textLength);
  div.style.backgroundColor = responsive.draw.colorToStr(color);
  return div;
}

responsive.render.section = function(divCount) {
  var section = document.createElement("section");
  section.style.backgroundColor = responsive.draw.colorToStr(responsive.math.randColor());
  for (var i = 0; i < divCount; i++) {
    section.appendChild(responsive.render.colorDiv(responsive.math.randIntInRange(30, 100)));
  }
  return section;
}
