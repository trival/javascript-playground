responsive.math.randIntInRange = function (min, max) {
  var range = max - min;
  var rand = responsive.math.randInt(range);
  return rand + min;
}

responsive.math.randInt = function (max) {
  return Math.floor(Math.random() * max);
}

responsive.math.randColor = function() {
  return [
    responsive.math.randInt(256),
    responsive.math.randInt(256),
    responsive.math.randInt(256),
    1
  ];
}
