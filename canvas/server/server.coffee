CLIENT_DIR = __dirname + '/../public'
SERVER_PORT = 7070

express = require 'express'

app = express()

app.configure ->
  app.use express.static(CLIENT_DIR)
  app.use express.errorHandler( dumpExceptions: true, showStack: true )

module.exports = app

app.listen SERVER_PORT
console.log "server started on port " + SERVER_PORT
