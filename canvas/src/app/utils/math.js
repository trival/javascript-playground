canvasTests.math.randIntInRange = function (min, max) {
  var range = max - min;
  var rand = Math.floor(Math.random() * range);
  return rand + min;
}
