canvasTests.helpers.extend = function(obj, mixin) {
  for(var key in mixin) {
    obj[key] = mixin[key];
  }
}
