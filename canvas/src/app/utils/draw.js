canvasTests.draw.colorToStr = function(c) {
  return "rgba(" + Math.floor(c[0]) + ","
                 + Math.floor(c[1]) + ","
                 + Math.floor(c[2]) + ","
                 + c[3] + ")"
}

/**
* @param {CanvasRenderingContext2D} ctx
*/
canvasTests.draw.radialGradient = function(ctx, pos, rad, color1, color2) {
  var colorToStr = canvasTests.draw.colorToStr;
  var x = pos[0], y = pos[1];
  var grad = ctx.createRadialGradient(x, y, 0, x, y, rad);
  grad.addColorStop(0, colorToStr(color1));
  grad.addColorStop(1, colorToStr(color2));
  ctx.fillStyle = grad;
  var twoRad = rad * 2;
  ctx.fillRect(x - rad, y - rad, twoRad, twoRad);
}
