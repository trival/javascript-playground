/** @constructor */
canvasTests.effects.Particle = function (options) {
  this.pos = [0, 0];
  this.vel = [0, 0];
  this.lifetime = 100;

  if(options)
    canvasTests.helpers.extend(this, options);

  this.life = 1;
  this.countdown = this.lifetime;
}


/** @constructor */
canvasTests.effects.ParticleSystem = function(options) {
  this.pos = [0, 0];
  this.velRange = [0, 20];
  this.lifeRange = vec2.set([], 100, 1000);
  this.startAcc = [0, 0];
  this.spreadSpeed = 0;
  this.startCount = 100;

  this.particleFactory = function () { return {} }

  if(options)
    canvasTests.helpers.extend(this, options);

  this.particles = [];
  for (var i = 0; i < this.startCount; i++) {
    this.particles.push(this.createParticle());
  };
}


canvasTests.effects.ParticleSystem.prototype.update = function (frameTime, acceleration) {
  var force, tmpVel;
  /** @type {Particle} */
  var p;

  var timeScale = frameTime / 1000;

  for (var i = this.particles.length - 1; i >= 0; i--) {
    p = this.particles[i];
    force = (typeof acceleration === "function") ?
            acceleration(p.pos) :
            vec2.copy([], acceleration);

    //update Position
    vec2.scale(force, force, timeScale);
    vec2.add(p.vel, p.vel, force);
    tmpVel = vec2.scale([],p.vel, timeScale);
    vec2.add(p.pos, p.pos, tmpVel);

    //update life
    p.countdown -= frameTime;
    p.life = p.countdown / p.lifetime;

    if (p.countdown <= 0)
      this.particles.splice(i, 1);
  };

  if (this.spreadSpeed > 0) {
    var newPs = Math.floor(this.spreadSpeed * timeScale);
    for (var i = newPs; i > 0; i--) {
      this.particles.push(this.createParticle());
    };
  }
}


canvasTests.effects.ParticleSystem.prototype.createParticle = function() {
  var lifetime = canvasTests.math.randIntInRange.apply(null, this.lifeRange);

  var velLength = canvasTests.math.randIntInRange.apply(null, this.velRange);
  var vel = [Math.random() - 0.5, Math.random() - 0.5];
  vec2.normalize(vel, vel);
  vec2.scale(vel, vel, velLength);
  vec2.add(vel, vel, this.startAcc);

  var options = {
    lifetime: lifetime,
    vel: vel,
    pos: vec2.copy([], this.pos)
  };

  canvasTests.helpers.extend(options, this.particleFactory());

  return new canvasTests.effects.Particle(options);
}
