canvasTests.particleApp1.state = {
  colorStart: [255, 150, 20, 1],
  colorEnd: [0, 0, 0, 1],
  color1: [],
  color2: [],
  acceleration: [0, -40],
  running: true,
  particleFactory: function() {
    var color = vec4.clone(canvasTests.particleApp1.state.colorStart);
    color[1] = Math.floor(Math.random() * color[1]);
    return {
      radius: Math.random() * 10,
      color: color
    }
  }
}


canvasTests.particleApp1.init = function() {
  glMatrix.setMatrixArrayType(Array);
  var state = canvasTests.particleApp1.state;

  var canvas = document.createElement("canvas");
  canvas.height = 600;
  canvas.width = 600;
  state.canvas = canvas;
  state.ctx = state.canvas.getContext("2d");
  state.ctx.globalCompositeOperation = "lighter";
  document.body.appendChild(canvas);

  state.last = Date.now();
  state.system = new canvasTests.effects.ParticleSystem({
    pos: [300, 500],
    lifeRange: [1000, 5000],
    startCount: 2000,
    spreadSpeed: 400,
    particleFactory: state.particleFactory
  });

  var stats = new Stats();
  stats.domElement.style.position = "absolute";
  stats.domElement.style.top = "0";
  stats.domElement.style.left = "0";
  document.body.appendChild(stats.domElement);
  state.stats = stats;
}


canvasTests.particleApp1.render = function() {
  var state = canvasTests.particleApp1.state;
  var now = Date.now();
  var frameTime = now - state.last;

  state.last = now;
  state.system.update(frameTime, state.acceleration);
  state.ctx.clearRect(0,0,state.canvas.width, state.canvas.height);

  for (var i = state.system.particles.length - 1; i >= 0; i--) {
    var p = state.system.particles[i];
    vec4.lerp(state.color1, state.colorEnd, p.color, p.life);
    vec4.copy(state.color2, state.color1);
    state.color1[3] = p.life;
    state.color2[3] = 0;
    var radius = p.radius * Math.pow((2 - p.life), 3);
    canvasTests.draw.radialGradient(state.ctx, p.pos, radius, state.color1, state.color2);
  };

  state.stats.update();

  if (state.running) requestAnimationFrame(canvasTests.particleApp1.render);
}


canvasTests.particleApp1.play = function() {
  canvasTests.particleApp1.state.running = true;
  canvasTests.particleApp1.render();
}

canvasTests.particleApp1.stop = function() {
  canvasTests.particleApp1.state.running = false;
}
