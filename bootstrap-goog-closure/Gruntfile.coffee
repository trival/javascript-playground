module.exports = (grunt) ->

  # load all grunt tasks
  require('matchdep').filterDev('grunt-*').forEach grunt.loadNpmTasks
  grunt.loadTasks 'utils/tasks'


  grunt.initConfig

    buildTempDir: "build/tmp/"
    buildDistDir: "build/dist/"
    closureDir: "custom_modules/closure-library"

    watch:
      options:
        livereload: true

      coffee:
        files: ['src/**/*.coffee', 'src/**/*.js']
        tasks: ['build', 'compileDev']

      less:
        files: ['src/style/{,*/}*.less']
        tasks: ['less', 'concat:css' ]

      html:
        files: ['public/**/*.html']
        tasks: []


    clean:
      dist: [
        'public/script/**/*'
        'pubic/style/*'
        'public/font/'
        '<%= buildTempDir %>'
        '<%= buildDistDir %>**/*'
      ]


    less:
      css:
        src : 'src/style/main.less'
        dest: '<%= buildDistDir %>style/main.css'


    copy:
      js:
        expand: true
        cwd: 'src/'
        src: '**/*.js'
        filter: 'isFile'
        dest: '<%= buildTempDir %>src/'

      dist:
        files:
          'public/script/app.js': '<%= buildDistDir %>app.js'
          # 'public/script/libs.js': '<%= buildDistDir %>libs.js'

      dev:
        files:
          '<%= buildDistDir %>app.js': '<%= buildDistDir %>app.dev.js'

      simple:
        files:
          '<%= buildDistDir %>app.js': '<%= buildDistDir %>app.simple.js'

      advanced:
        files:
          '<%= buildDistDir %>app.js': '<%= buildDistDir %>app.advanced.js'

      assetFonts:
        expand: true
        cwd: 'bower_modules/font-awesome/font/'
        src: '*'
        flatten: true
        filter: 'isFile'
        dest: 'public/font/'


    concat:
      modules:
        files:
         '<%= buildDistDir %>app.build.js': [
            'utils/modules.js'
            '<%= buildDistDir %>app.raw.js'
          ]

      compile:
        files:
          '<%= buildDistDir %>app.simple.js': '<%= buildDistDir %>app.simple.js'
          '<%= buildDistDir %>app.advanced.js': '<%= buildDistDir %>app.advanced.js'
        options:
          banner: '(function(){'
          footer: '}).call(typeof(window)!=="undefined"?window:(typeof(global)!=="undefined"?global:this))'

      # libs:
      #   src: [
      #     'bower_modules/angular/angular.min.js'
      #     ...
      #   ]
      #   dest: '<%= buildDistDir %>libs.js'

      css:
        src: [
          # 'bower_modules/bootstrap-css/css/bootstrap.min.css'
          '<%= buildDistDir %>style/*'
        ]
        dest: 'public/style/style.css'


    # needed vor node in dev mode
    globalifyGoog:
      dev:
        files:
          '<%= buildDistDir %>app.dev.js': '<%= buildDistDir %>app.build.js'


    coffee:
      app:
        options:
          bare: true
        files: [
          expand: true
          src: 'src/**/*.coffee'
          dest: '<%= buildTempDir %>'
          ext: '.js'
        ]


    closureBuilder:
      options:
        closureLibraryPath: '<%= closureDir %>'
      app:
        options:
          # namespaces: ['trivialspace.editor.ui.app'] # namespaces
          inputs: [
            '<%= buildTempDir %>src/**/*.js'
          ]
          output_mode: 'script',
        src: ['<%= buildTempDir %>', '<%= closureDir %>/']
        dest: '<%= buildDistDir %>app.raw.js'


    closurecompiler:
      options:
        formatting: 'PRETTY_PRINT'
      # whitespace:
      #   src: '<%= buildDistDir %>src.build.js'
      #   dest: '<%= buildDistDir %>app.whitespace.js'
      #   options:
      #     compilation_level: 'WHITESPACE_ONLY'
      simple:
        files:
          '<%= buildDistDir %>app.simple.js': '<%= buildDistDir %>app.build.js'
        options:
          compilation_level: 'SIMPLE_OPTIMIZATIONS'
      advanced:
        files:
          '<%= buildDistDir %>app.advanced.js': '<%= buildDistDir %>app.build.js'
        options:
          compilation_level: 'ADVANCED_OPTIMIZATIONS'


    nodemon:
      server:
        options:
          file: 'server/server.js'
          # ignoredFiles: ['server-config-template.coffee']
          watchedExtensions: ['js', 'coffee']
          watchedFolders: ['server']
          debug: true
          delayTime: 1


    concurrent:
      target:
        tasks: ['nodemon', 'watch']
        options:
          logConcurrentOutput: true


    mochaTest:
      node:
        src: ['test/nodejs/**/*.coffee', 'test/shared/**/*.coffee']
        options:
          reporter: 'spec'
          ui: 'tdd'
          timeout: 5000
          require: 'test/nodejs-setup.js'


    karma:
      unit:
        configFile: 'karma.conf.coffee'


  # build/dist tasks
  grunt.registerTask 'build', ['coffee', 'copy:js', 'closureBuilder', 'concat:modules']
  grunt.registerTask 'distResources', ['less', 'copy:assetFonts', 'concat:css', 'copy:dist']
  grunt.registerTask 'compileDev', ['globalifyGoog', 'copy:dev', 'copy:dist']
  grunt.registerTask 'compileSimple', ['closurecompiler:simple', 'concat:compile', 'copy:simple', 'copy:dist']
  grunt.registerTask 'compileAdvanced', ['closurecompiler:advanced', 'concat:compile', 'copy:advanced', 'copy:dist']
  grunt.registerTask 'distDev', ['clean', 'build', 'compileDev', 'distResources']
  grunt.registerTask 'distSimple', ['clean', 'build', 'compileSimple', 'distResources']
  grunt.registerTask 'distAdvanced', ['clean', 'build', 'compileAdvanced', 'distResources']

  # test tasks
  grunt.registerTask 'test', ['mochaTest', 'karma']

  # automated dev tasks
  grunt.registerTask 'default', ['distDev', 'concurrent']
