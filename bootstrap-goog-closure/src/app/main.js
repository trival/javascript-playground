goog.provide('myapp.main');

myapp.main = function(name) {
  var p = document.createElement("p"),
      greet = "hello " + name;
  p.innerHTML = greet;
  document.body.appendChild(p);
  console.log(greet);
};
