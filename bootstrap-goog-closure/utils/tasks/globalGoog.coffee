module.exports = (grunt) ->
  "use strict"

  linefeed = grunt.util.linefeed

  grunt.registerMultiTask "globalifyGoog", "make goog available on the global object", ->

    @files.forEach (f) ->
      validFiles = f.src.filter filesFilter
      writeFile f.dest, concatOutput validFiles


  filesFilter = (filepath) ->
    unless grunt.file.exists filepath
      grunt.log.warn "File #{filepath} not found."
      false
    else
      true


  concatOutput = (files) ->
    files.map((filepath) ->
      code = grunt.file.read filepath
      globalifyGoog code
    ).join linefeed


  globalifyGoog = (code) ->
    regex = /\s*(goog\.global\s*=\s*this\s*;)/g
    code.replace regex, "$1\n
    if (typeof global !== 'undefined') {
      global.goog = goog;
      goog.global = global;
    }
    "


  warnOnEmptyFile = (path) ->
    grunt.log.warn "Destination #{path} not written because compiled files were empty."


  writeFile = (path, output) ->
    if output.length < 1
      warnOnEmptyFile path
    else
      grunt.file.write path, output
      grunt.log.writeln "File #{path} created."
