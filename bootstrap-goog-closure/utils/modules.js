function defns(ns, deps, fn) {
  if (typeof deps === 'function') {
    deps.call(ns);
  } else {
    fn.apply(ns, deps);
  }
};
