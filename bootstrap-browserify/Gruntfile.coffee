module.exports = (grunt) ->

  # load all grunt tasks
  require('matchdep').filterDev('grunt-*').forEach grunt.loadNpmTasks


  grunt.initConfig

    buildTempDir: "build/tmp/"
    buildDistDir: "build/dist/"
    bowerLibs: "bower_modules/"

    watch:
      options:
        livereload: true

      coffee:
        files: ['src/**/*.coffee', 'src/**/*.js']
        tasks: ['build', 'distJs']

      less:
        files: ['src/style/{,*/}*.less']
        tasks: ['less', 'concat:css' ]

      html:
        files: ['public/**/*.html']
        tasks: []


    clean:
      dist: [
        'public/script/**/*'
        'pubic/style/*'
        'public/font/'
        '<%= buildTempDir %>'
        '<%= buildDistDir %>**/*'
      ]


    less:
      css:
        src : 'src/style/main.less'
        dest: '<%= buildDistDir %>style/main.css'


    copy:
      js:
        expand: true
        cwd: 'src/'
        src: '**/*.js'
        filter: 'isFile'
        dest: '<%= buildTempDir %>src/'

      dist:
        files:
          'public/script/app.js': '<%= buildDistDir %>app.js'
          'public/script/libs.js': '<%= buildDistDir %>libs.js'

      build:
        files:
          '<%= buildDistDir %>app.js': '<%= buildDistDir %>app.build.js'
          '<%= buildDistDir %>libs.js': '<%= buildDistDir %>libs.build.js'

      assetFonts:
        expand: true
        cwd: 'bower_modules/font-awesome/font/'
        src: '*'
        flatten: true
        filter: 'isFile'
        dest: 'public/font/'


    concat:
      css:
        src: [
          # 'bower_modules/bootstrap-css/css/bootstrap.min.css'
          '<%= buildDistDir %>style/*'
        ]
        dest: 'public/style/style.css'


    coffee:
      app:
        options:
          bare: true
        files: [
          expand: true
          src: 'src/**/*.coffee'
          dest: '<%= buildTempDir %>'
          ext: '.js'
        ]


    browserify:

      libs:
        dest: '<%= buildDistDir %>libs.build.js'
        src: []
        options:
          alias: [
            "lodash:"
          ]
          shim:
            jquery:
              path: "<%= bowerLibs %>jquery/jquery.min.js"
              exports: '$'
            bootstrap:
              path: "<%= bowerLibs %>bootstrap/dist/js/bootstrap.min.js"
              exports: null
              depends: jquery: 'jQuery'

      app:
        src: ['<%= buildTempDir %>src/app/run.js']
        dest: '<%= buildDistDir %>app.build.js'
        options:
          alias: [
            "<%= bowerLibs %>jquery/jquery.min.js:jquery"
            "<%= bowerLibs %>bootstrap/dist/js/bootstrap.min.js:bootstrap"
          ]
          external: [
            "<%= bowerLibs %>jquery/jquery.min.js"
            "<%= bowerLibs %>bootstrap/dist/js/bootstrap.min.js"
            "lodash"
          ]


    nodemon:
      server:
        options:
          file: 'server/server.js'
          # ignoredFiles: ['server-config-template.coffee']
          watchedExtensions: ['js', 'coffee']
          watchedFolders: ['server']
          debug: true
          delayTime: 1


    concurrent:
      target:
        tasks: ['nodemon', 'watch']
        options:
          logConcurrentOutput: true


    mochaTest:
      node:
        src: ['test/nodejs/**/*.coffee', 'test/shared/**/*.coffee']
        options:
          reporter: 'spec'
          ui: 'tdd'
          timeout: 5000
          require: 'test/nodejs-setup.js'


    karma:
      unit:
        configFile: 'karma.conf.coffee'


  # build/dist tasks
  grunt.registerTask 'build', ['coffee', 'copy:js', 'browserify']
  grunt.registerTask 'distResources', ['less', 'copy:assetFonts', 'concat:css']
  grunt.registerTask 'distJs', ['copy:build', 'copy:dist']
  grunt.registerTask 'distDev', ['clean', 'build', 'distResources', 'distJs']

  # test tasks
  grunt.registerTask 'test', ['mochaTest', 'karma']

  # automated dev tasks
  grunt.registerTask 'default', ['distDev', 'concurrent']
