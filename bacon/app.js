var CELL_COUNT = 20,
    CELL_WIDTH = 20,
    frames = [],
    screen = document.getElementById('screen');
    
screen.width = screen.height = CELL_COUNT * CELL_WIDTH;

var ctx = screen.getContext('2d'),
    x0 = screen.offsetLeft,
    y0 = screen.offsetTop;


// functions
function screenIndex (x, y) {
    return y * CELL_COUNT + x;
}

function screenPos (index) {
    var x = index % CELL_COUNT,
        y = Math.floor(index / CELL_COUNT);
    return [x, y];
}

function cellId (x, y) {
    return 'px' + x + '_' + y;
}

function toCellPos (e) {
	var x = e.clientX - x0 + document.body.scrollLeft,
        y = e.clientY - y0 + document.body.scrollTop;
    x = Math.floor(x / CELL_WIDTH);
    y = Math.floor(y / CELL_WIDTH);
    return [x, y];
}

function isEqualPos (pos1, pos2) {
	return pos1[0] === pos2[0] && pos1[1] === pos2[1];
}

function changeFrameIndex (prev, nextObj) {
    if (prev >= nextObj.length) prev = nextObj.length - 1;
    prev = (prev <= 0) ? nextObj.length : prev;
    return (prev + nextObj.offset) % nextObj.length;
}

function makeNextIndexObj (offset) {
    return function (value) {
        return {length: value, offset: offset};
    };
}

function makeNextObj (offset, length) {
    return {offset: offset, length: length};
}

function updateFramesLength (old, next) {
    return Math.max(1, old + next);
}

function copyFrame (oldFrame) {
 	var i, newFrame = createFrame();
    for (i in oldFrame) {
    	newFrame[i] = oldFrame[i];
    }
    return newFrame;
}

function createFrame () {
	return new Uint8Array(CELL_COUNT * CELL_COUNT);
}

// frame callbacks
function intoFrames (frame, index) {
	frames.splice(index, 0, frame);
}

function deleteFrame (index) {
    frames.splice(index, 1);
}

// callbacks
function drawScreen (frame) {
    var i, pos, on;
    for (i in frame) {
        on = frame[i];
        pos = screenPos(i);
        drawCell(pos, on);
    }
} 

function updateCell (pos, frame) {
    var i = screenIndex(pos[0], pos[1]),
        on = frame[i] = !frame[i];

    drawCell(pos, on);
}

function drawCell (pos, on) {
    var x = pos[0] * CELL_WIDTH, 
        y = pos[1] * CELL_WIDTH;
    if (on) {
        ctx.fillStyle = '#000';
    } else {
        ctx.fillStyle = '#fff';
    }
    ctx.fillRect(x, y, CELL_WIDTH, CELL_WIDTH);
}

// init frames
frames.push(createFrame());

// Event Streams
var mouseDown = $(screen).asEventStream("mousedown").doAction(".preventDefault"),
    mouseUp = $(document).asEventStream("mouseup").doAction(".preventDefault"),
    mouseMoves = $(screen).asEventStream('mousemove').map(toCellPos).skipDuplicates(isEqualPos),
    clicks = mouseDown.map(toCellPos),
    cellTouches = mouseDown.flatMap(function () {
                return mouseMoves.takeUntil(mouseUp);
            }).merge(clicks),
    newStream = $('#new-btn').asEventStream('click'),
    copyStream = $('#copy-btn').asEventStream('click'),
    deleteStream = $('#del-btn').asEventStream('click'),
    nextIndex = $('#next-btn').asEventStream('click'),
    prevIndex = $('#prev-btn').asEventStream('click'),
    play = $('#play-btn').asEventStream('click')
            .scan(false, function (prev) {return !prev;}).changes(),
    stop = play.filter(function (v) {return !v}),
    animation = play.filter(function (v) {return v})
            .flatMap(function () {
                return Bacon.interval(200, 1).takeUntil(stop);
            })
    framesLengthBus = new Bacon.Bus(),
    framesLength = framesLengthBus.toProperty(frames.length),
    currentIndex = nextIndex.merge(animation).map(framesLength).map(makeNextIndexObj(1))
            .merge(prevIndex.map(framesLength).map(makeNextIndexObj(-1)))
            .merge(framesLengthBus.map(makeNextIndexObj(0)))
            .scan(0, changeFrameIndex),
    frame = currentIndex.map(function (i) {return frames[i];}),
    changes = frame.sampledBy(cellTouches, function (frame, pos) {
                return {frame: frame, pos: pos};
            });

frame.onValue(drawScreen);
changes.onValue(function (o) {updateCell(o.pos, o.frame);});
newStream.map(currentIndex).onValue(function (i) {
    intoFrames(createFrame(), i);
    framesLengthBus.push(frames.length);
});
copyStream.map(currentIndex).onValue(function (i) {
    intoFrames(copyFrame(frames[i]), i);
    framesLengthBus.push(frames.length);
});
deleteStream.map(currentIndex).onValue(function (i) {
    if (frames.length > 1) {
        deleteFrame(i);
        framesLengthBus.push(frames.length);
    }
});
currentIndex.onValue(function (v) {console.log('currentIndex', v)})
framesLength.onValue(function (v) {console.log('framesLength', v)})
